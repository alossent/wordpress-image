#!/bin/bash

function copy_resources_to_wp_content () {
    # Copy gitlab provided plugins, themes files into the persistent volume. 
    echo "--> Copying custom themes and plugins ..."
    if [ -d /opt/app-root/downloads/plugins ]; then
        cp -rf /opt/app-root/downloads/plugins/* wp-content/plugins/ 2>/dev/null || true
    fi
    if [ -d /opt/app-root/downloads/themes ]; then
        cp -rf /opt/app-root/downloads/themes/* wp-content/themes/ 2>/dev/null || true
    fi
}

if ! $(wp core is-installed); then
    ###################### New install ######################
    echo "--> Wordpress is not installed. Setting up ..."
    wp core download
    # Verify Checksums
    wp core verify-checksums

    # Copy gitlab provided plugins, themes files into the persistent volume. 
    copy_resources_to_wp_content

    echo "--> Creating Wordpress configuration files ..."
    # --skip-check avoids the need for mysql binary and mysql lcoal install
    wp config create --dbname=$MYSQL_DATABASE --dbhost=$MYSQL_HOST --dbprefix=$MYSQL_TABLE_PREFIX --dbuser=$MYSQL_USER --dbpass=$MYSQL_PASSWORD --skip-check
    # Enforse SSL
    wp config set FORCE_SSL_ADMIN true --raw
    # Fix http to https endless loop crap impossible to replace in the DB
    sed -i "/'FORCE_SSL_ADMIN', *true *);/a\
    if ( isset( \$_SERVER['HTTP_X_FORWARDED_PROTO'] ) && \$_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') \$_SERVER['HTTPS']='on';" wp-config.php

    echo "--> Wordpress site set up"
    # Ensure the DB is up and running
    until wp db check
    do
        echo "  --> Waiting for database connection..."
        # wait for 5 seconds before check again
        sleep 5
    done

    # wait for 10 seconds before continuing
    echo "  --> Waiting 10s..."
    sleep 10
    
    # Create Wordpress instance and admin account
    # Until https://github.com/oidc-wp/openid-connect-generic/pull/256 gets merged,
    # there is no way to set the scope to `openid` through environment variables like the clientId, etc.
    # Hence, we force users to put an admin password mandatory in order to configure OIDC accordingly.
    # When PR is merged, we can consider remove the --admin_password flag and configure OIDC from the beginning.
    wp core install --url=https://$APPLICATION_NAME --title=$APPLICATION_NAME --admin_user=admin --admin_password=$ADMIN_PASSWORD --admin_email=$ADMIN_EMAIL --skip-email
    # Enable english language    
    wp site switch-language en_US

    # Enable openid plugin
    wp plugin activate openid-connect-generic

    # Remove ping_sites like pingomatic (settings - writing - update services)
    wp option delete ping_sites
    
    # Update plugins
    wp plugin update --all
else
    ###################### Update install if needed ######################
    echo "Wordpress site is already setup according to wp core is-installed command"

    # Copy gitlab provided plugins, themes files into the persistent volume. 
    copy_resources_to_wp_content

    #if ! $(/opt/app-root/bin/wp core check-update); then
    if ! [[ $(wp core check-update) == *"WordPress is at the latest version"* ]] 2>/dev/null; then
        echo "Update is needed, proceeding"
        # Update WP core files
        wp core update
        # Update DB eventually
        wp core update-db
        # Verify Checksums
        wp core verify-checksums
    fi
    # Update plugins
    wp plugin update --all
fi

# Use exec for invoking it to ensure signals are handled properly.
exec /usr/libexec/s2i/run