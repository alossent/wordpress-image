=== Indico Widget Plugin ===
Contributors: Emmanuel Ormancey
Tags: Indico
Requires at least: 4.6
Tested up to: 5.2
Stable tag: 4.3
Requires PHP: 5.2.4
License: GPLv3
License URI: https://www.gnu.org/licenses/gpl-3.0.html

Indico widget plugin. Displays Indico events or room bookings on a wordpress site.

== Description ==

Indico widget plugin. Displays Indico events or room bookings on a wordpress site.

== Installation ==

Install and enable plugin.
Add one or several widget(s), and configure them independantly.
From and To dates are either the string <i>today</i> or a number of days before or after today speficied with <i>+x day</i> or <i>-x day</i>.


== Changelog ==

= 1.1 =
* Minor bugfixes.

= 1.0 =
* Initial version.

== Upgrade Notice ==

